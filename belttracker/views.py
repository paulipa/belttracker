from django.shortcuts import render
from django.contrib.auth.decorators import login_required, permission_required

from .models import Belt

@login_required
def index(request):
    
    belts = []
    for belt in Belt.objects.all():
        belts.append(belt)

    context = {
        'belts': belts
    }    

    return render(request, 'belttracker/index.html', context)


@login_required
@permission_required('belttracker.mining_fc')
def clear(request, id):
    belt = Belt.objects.get(id=id)
    belt.cleared = True
    belt.save()
    belts = []
    for belt in Belt.objects.all():
        belts.append(belt)

    context = {
        'belts': belts
    }

    return render(request, 'belttracker/index.html', context)


@login_required
@permission_required('belttracker.mining_fc')
def reset(request, id):
    belt = Belt.objects.get(id=id)
    belt.cleared = False
    belt.save()
    belts = []
    for belt in Belt.objects.all():
        belts.append(belt)

    context = {
        'belts': belts
    }

    return render(request, 'belttracker/index.html', context)


@login_required
@permission_required('belttracker.mining_fc')
def reset_all(request):
    belts = []
    for belt in Belt.objects.all():
        belt.cleared = False
        belt.save()
        belts.append(belt)

    context = {
        'belts': belts
    }

    return render(request, 'belttracker/index.html', context)


