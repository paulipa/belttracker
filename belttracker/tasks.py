from celery import shared_task
import logging
from .models import Belt


logger = logging.getLogger(__name__)

# Create your tasks here

@shared_task
def reset_belts():
    for belt in Belt.objects.all():
        belt.cleared = False
        belt.save()
    return

"""
Example task:

@shared_task
def my_task():
    pass
"""
   