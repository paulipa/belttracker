from django.apps import AppConfig


class ExampleConfig(AppConfig):
    name = 'belttracker'
    label = 'belttracker'
    verbose_name = 'Belt Tracker'
