from django.db import models


# Create your models here.

class BeltTracker(models.Model):
    """Meta model for app permissions"""

    class Meta:
        managed = False                         
        default_permissions = ()
        permissions = ( 
            ('basic_access', 'Can access this app'), 
            ('mining_fc', 'Can administer the status of belts')
        )

class Belt(models.Model):
    """Model for an actual belt"""

    belt_name = models.CharField(max_length=200)
    system_name = models.CharField(max_length=200)
    cleared = models.BooleanField(default = False)
    last_updated = models.DateTimeField(auto_now=True)
    estimated_rocks = models.IntegerField(default=0)
