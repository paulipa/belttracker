# About

This plugin helps mining fleets track belts they have already mined.
It has a button to mass reset belt status to not mined after DT.
This process can be automated through celery tasks, see below for details.

![Screenshot](https://i.imgur.com/Bm1LdzT.png)

# Installation

To install the plugin activate the virtual environment of your auth and type
in the following.

```
pip install git+https://gitlab.com/mani24/belttracker.git
python manage.py migrate belttracker
```

To automate belt resets add the following to your `local.py`:
```
# settings for belttracker
CELERYBEAT_SCHEDULE['belttracker_reset_belts'] = {
    'task': 'belttracker.tasks.reset_belts',
    'schedule': crontab(hour=[12]),
}

```


Then restart auth most likely with:

```
sudo supervisorctl restart myauth:
```
